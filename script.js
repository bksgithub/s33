/*
3. Create a fetch request using the GET method that will retrieve all the
to do list items from JSON Placeholder API*/

/*
4. Using the data retrieved, create an array using the map method to
return just the title of every item and print the result in the console.*/

let titles = [];
let apiArray = fetch("https://jsonplaceholder.typicode.com/todos").then((response) => response.json()).then((data) => {
	data.map((elements) => {
		titles.push(elements.title);
	});
})

console.log(apiArray);
console.log(titles);

/*
6. Using the data retrieved, print a message in the console that will
provide the title and status of the to do list item.*/

let getTitleAndStatus = fetch("https://jsonplaceholder.typicode.com/todos/1").then((response) => response.json()).then((data) => {
	console.log(data.title, data.completed);
})

console.log(getTitleAndStatus);

/*
7. Create a fetch request using the POST method that will create a to
do list item using the JSON Placeholder API.
*/
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New To do List",
		body: "This is a new to do list item",
		userId: 1
	})
}).then((response) => response.json()).then((data) => {
	console.log(data);
})

/*
8. Create a fetch request using the PUT method that will update a to do
list item using the JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated to do list",
		body: "This is the updated to do list item"
	})
}).then((response) => response.json()).then((data) => {
	console.log(data);
})

/*
9. Update a to do list item by changing the data structure to contain
the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID*/

/*
10. Create a fetch request using the PATCH method that will update a to
do list item using the JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated to do list",
		description: "This is the updated to do list item",
		status: "",
		dateCompleted: "",
		userId: 1
	})
}).then((response) => response.json()).then((data) => {
	console.log(data);
})


/*
11. Update a to do list item by changing the status to complete and add
a date when the status was changed.
*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated to do list",
		description: "This is the updated to do list item",
		status: "complete",
		dateCompleted: new Date(),
		userId: 1
	})
}).then((response) => response.json()).then((data) => {
	console.log(data);
})

/*
12. Create a fetch request using the DELETE method that will delete an
item using the JSON Placeholder API*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})

/*
Create a request via Postman to retrieve all the to do list items.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list items*/